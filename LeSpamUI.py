#!/usr/bin/python3
import tkinter as tk
import tkinter.ttk as ttk

import re

class LeSpamUI:
    def __init__(self, master=None):
        # build ui
        self.main_frame = ttk.Frame(master)
        self.main_frame.configure(height=200, width=200)
        self.message_title = ttk.Entry(self.main_frame)
        self.message_title.grid(column=0, row=0)
        self.message_text = tk.Text(self.main_frame)
        self.message_text.configure(height=10, width=50)
        self.message_text.grid(column=0, columnspan=2, row=1)
        self.reciever_button = ttk.Button(self.main_frame)
        self.reciever_button.configure(text="Select recievers")
        self.reciever_button.grid(column=1, row=0)
        self.reciever_button.bind("<ButtonPress>", self.onRecieverSelect, add="")
        self.send_button = ttk.Button(self.main_frame)
        self.send_button.configure(text="Send")
        self.send_button.grid(column=0, row=7)
        self.send_button.bind("<ButtonPress>", self.onSend, add="")
        interger_vcmd = (self.main_frame.register(self.integerFilter),
                         '%P')
        self.thread_count = ttk.Entry(self.main_frame, validate='key', validatecommand=interger_vcmd)
        self.thread_count.grid(column=1, row=5)
        self.interrupt_button = ttk.Button(self.main_frame)
        self.interrupt_button.configure(text="Interrupt")
        self.interrupt_button.grid(column=1, row=7)
        self.interrupt_button.bind("<ButtonPress>", self.onInterrupt, add="")
        self.thread_label = ttk.Label(self.main_frame)
        self.thread_label.configure(text="Threads")
        self.thread_label.grid(column=0, row=5)
        self.sent_count_label = ttk.Label(self.main_frame)
        self.sent_count_label.configure(text="Messages sent")
        self.sent_count_label.grid(column=0, row=6)
        self.seperator = ttk.Separator(self.main_frame)
        self.seperator.configure(orient="horizontal")
        self.seperator.grid(column=0, columnspan=2, pady=10, row=3, sticky="ew")
        self.sent_counter = ttk.Label(self.main_frame)
        self.sent_counter.configure(text="0")
        self.sent_counter.grid(column=1, row=6)
        self.main_frame.grid(column=0, row=0)
        self.main_frame.rowconfigure("all", pad=10)
        self.main_frame.columnconfigure("all", pad=10)

        # Main widget
        self.mainwindow = self.main_frame

    def run(self):
        self.mainwindow.mainloop()

    def onRecieverSelect(self, event=None):
        pass

    def onSend(self, event=None):
        pass

    def integerFilter(self, string: str):
        if re.match("^\\d*$", string) or len(string) == 0:
            return True
        else:
            return False

    def onInterrupt(self, event=None):
        pass


if __name__ == "__main__":
    root = tk.Tk()
    root.resizable(False, False)
    root.title('LeSpam')
    app = LeSpamUI(root)
    app.run()
