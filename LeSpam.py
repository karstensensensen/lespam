from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep
import re
import json
from urllib.parse import parse_qs, urlencode

def selectRecievers():
    caps = DesiredCapabilities.CHROME
    caps['goog:loggingPrefs'] = {'performance': 'ALL'}
    chrome_driver = webdriver.Chrome(desired_capabilities=caps)

    chrome_driver.get("https://www.lectio.dk/")

    header_data = None
    post_data = None

    found_message_post = False

    while not found_message_post:
        # wait until the message page is reached
        while not re.match("^https://www\\.lectio\\.dk/lectio/\\d*/beskeder2\\.aspx\\?type=nybesked&.*$", chrome_driver.current_url):
            sleep(0.5)

        # now wait until the message page is left, as this means either a message has been sent, or a message was cancled
        while not re.match("^https://www\\.lectio\\.dk/lectio/\\d*/beskeder2\\.aspx\\?type=&.*$", chrome_driver.current_url):
            sleep(0.5)

        for entry in chrome_driver.get_log('performance'):
            message = entry['message']

            try:
                message = json.loads(message)['message']
            except Exception as e:
                print(message)
                raise e

            if 'params' in message and 'request' in message['params'] and 'hasPostData' in message['params']['request']\
                    and message['params']['request']['hasPostData']:

                post_data = parse_qs(message['params']['request']['postData'])

                if 's$m$Content$Content$CreateThreadEditMessageOkBtn' in post_data['__EVENTTARGET']:
                    found_message_post = True
                    header_data = message['params']['request']['headers']
                    break

        if not found_message_post:
            print("No message request found (user aborted?)")


    print(post_data)
    print(header_data)

    print(urlencode(post_data))

    chrome_driver.quit()

    return header_data, post_data


print(selectRecievers())
